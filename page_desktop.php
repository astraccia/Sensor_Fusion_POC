<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1" />
		<style type="text/css">

			*{
				margin:0;
				padding:0;
			}
			html, body{
				height:100%;
				overflow:hidden;
			}

			canvas {
				width: 100%;
				height: 100%;
			}

		.panel{

			
		margin-right: 3px;
		}

		.button {
		    background-color: #4CAF50;
		    border: none;
		    color: white;
			margin-right: 30%;   
			margin-left: 30%;
		    text-decoration: none;
		    display: block;
		    font-size: 16px;
		    cursor: pointer;
			width:30%;
		    height:40px;
			margin-top: 5px;
			 
		}
		input[type=text]{
				width:100%;
				margin-top:5px;
				
			}


		.chat_wrapper {
			width: 300PX;
			height:auto;
			margin-right: auto;
			margin-left: auto;
			background: #3B5998;
			border: 1px solid #999999;
			padding: 10px;
			font: 14px 'lucida grande',tahoma,verdana,arial,sans-serif;
			position: absolute;
			z-index: 999999999;
			top: 0;
			display: none;
		}
		.chat_wrapper .message_box {
			background: #F7F7F7;
			height:350px;
			overflow: auto;
			padding: 10px 10px 20px 10px;
			border: 1px solid #999999;
		}
		.chat_wrapper  input{
			//padding: 2px 2px 2px 5px;
		}
		.system_msg{color: #BDBDBD;font-style: italic;}
		.user_name{font-weight:bold;}
		.user_message{color: #88B6E0;}

		@media only screen and (max-width: 720px) {
		    /* For mobile phones: */
		    .chat_wrapper {
		        width: 95%;
			height: 40%;
			}
		    

			.button{ width:100%;
			margin-right:auto;   
			margin-left:auto;
			height:40px;}

		}

		</style>

		<script language="javascript" type="text/javascript">  
			var init;
			var screenOrientation, orient;	
			var object1, object2, player;	
			var container, camera, scene, renderer, controls, controlsOrbit, geometry, mesh;
			var msg = new Object();
			var firstLoad = true;
			var alphaIni, betaIni, gammaIni;
			var xIni, yIni, zIni;
			var clients = new Array();
			var collidableMeshList = [];
			</script>
	</head>


	<body>	

			<script src="js/jquery-3.1.1.js"></script>
			<script src="js/threejs/three.js"></script>
			<script src="js/threejs/DeviceOrientationControls.js"></script>
			<script src="js/threejs/OrbitControls.js"></script>
			<script src="js/threejs/STLLoader.js"></script>
			<script src="js/threejs/OBJLoader.js"></script>
			<script src="js/threejs/MTLLoader.js"></script>
			<script src="js/threejs/ColladaLoader.js"></script>
			<script src="js/cannon/cannon.min.js"></script>

			<script language="javascript" type="text/javascript">  

				init = function() {

						scene = new THREE.Scene();
						scene.background = new THREE.Color( 0xffffff );
						renderer = new THREE.WebGLRenderer({antialias:true});
						renderer.setPixelRatio( window.devicePixelRatio );
						renderer.setSize( window.innerWidth, window.innerHeight );
						container = document.getElementById( 'container' );
						container.appendChild( renderer.domElement );
						
						camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.1, 10000000 );
						camera.position.z = 500;
						camera.fov = 70;

						var light1 = new THREE.PointLight( 0xffffff, 0.7, 0 );
						light1.position.set(-50,100,140);
						light1.castShadow = true;
						scene.add(light1);

						var light2 = new THREE.PointLight( 0xffffff, 0.7, 0 );
						light2.position.set(150,100,-140);
						light2.castShadow = true;
						scene.add(light2);

						var light3 = new THREE.PointLight( 0xffffff, 0.7, 0 );
						light3.position.set(150,-100,-120);
						light3.castShadow = true;
						scene.add(light3);

						

				
						// controlsOrbit = new THREE.OrbitControls( camera, renderer.domElement );
						// controlsOrbit.enabled = true;
						// controlsOrbit.minDistance = 0;
						// controlsOrbit.maxDistance = 1800;
						// controlsOrbit.update();


						// var objLoader = new THREE.OBJLoader();
						// var texture = new THREE.TextureLoader().load( "models/textures/phone.jpg" );
						// objLoader.load( 'models/SwordMinecraft.obj', function ( object ) {
							

						// 	object.traverse( function ( child ) {
						// 		if ( child instanceof THREE.Mesh ) {
						// 			child.castShadow = true;
						// 			child.receiveShadow = true;

						// 			child.material = new THREE.MeshPhongMaterial( {map: texture, color: 0xeeeeee, specular: 0xffffff, shininess: 100 } );
						// 		}
						// 	} );

						// 	object.position.set(0,0,0);
						// 	object.scale.set(10,10,10);
						// 	scene.add(object);
						// 	object1 = object;
						// });


						var loader = new THREE.STLLoader();
						loader.load( './models/Marbo_front_stl.stl', function ( geometry ) {
							var texture = new THREE.TextureLoader().load( "models/textures/Marbo_front_texture.png" );
							var material = new THREE.MeshPhongMaterial( { map: texture, specular: 0x111111, shininess: 200 } );
							var mesh = new THREE.Mesh( geometry, material );
							// mesh.rotation.x = -Math.PI/2;
							mesh.castShadow = true;
							mesh.receiveShadow = true;

							scene.add( mesh );
							object1 = mesh;
							object1.rotation.order = "YXZ";
							mesh.scale.set( 0.1, 0.1, 0.1 );

						} );

						var loader = new THREE.STLLoader();
						loader.load( './models/Marbo_back_stl.stl', function ( geometry ) {
							var texture = new THREE.TextureLoader().load( "models/textures/Marbo_back_texture.png" );
							var material = new THREE.MeshPhongMaterial( { map: texture, specular: 0x111111, shininess: 200 } );
							var mesh = new THREE.Mesh( geometry, material );
							// mesh.rotation.x = -Math.PI/2;
							mesh.castShadow = true;
							mesh.receiveShadow = true;

							scene.add( mesh );
							object2 = mesh;
							object2.rotation.order = "YXZ";
							mesh.scale.set( 0.1, 0.1, 0.1 );

						} );


						var map = new THREE.TextureLoader().load("models/orca.png");
						var geometryLogo = new THREE.PlaneGeometry(1024/20, 1200/20);
						var materialLogo = new THREE.MeshPhongMaterial( { map: map, transparent: true } );

						orca = new THREE.Mesh (geometryLogo, materialLogo);
						orca.material.side = THREE.DoubleSide; 
						orca.position.set(-150,-50, 0);
						scene.add(orca);

						orca2 = new THREE.Mesh (geometryLogo, materialLogo);
						orca2.material.side = THREE.DoubleSide; 
						orca2.position.set(110,25, 0);
						scene.add(orca2);

						orca3 = new THREE.Mesh (geometryLogo, materialLogo);
						orca3.material.side = THREE.DoubleSide; 
						orca3.position.set(40,115, 0);
						scene.add(orca3);



						collidableMeshList.push(orca, orca2);



						var render = function () {
							width = $(window).width();
							height = $(window).height();

							if (msg.data) {
								if (msg.client == clients[0]) {
									movePlayer(object1);
								} else if (msg.client == clients[1]) {
									movePlayer(object2);
								}
							}

							

							

							requestAnimationFrame( render );
							renderer.render(scene, camera);
						};


						var movePlayer = function(player) {
							player.rotation.x = degreeToRad(msg.data.beta - betaIni);
							player.rotation.y = degreeToRad(msg.data.alpha - alphaIni);
							player.rotation.z = degreeToRad(-msg.data.gamma - gammaIni);


							player.position.x += (msg.data.acceleration_x.toFixed(1)*2);
							if (player.position.x > width/5 ) {
								player.position.x = width/5;
							} else if (player.position.x < -width/5 ) {
								
								player.position.x = -width/5;
							}

							player.position.y -= (msg.data.acceleration_y.toFixed(1)*2);
							if (player.position.y > height/5 ) {
								player.position.y = height/5;
							} else if (player.position.y < -height/5 ) {
								
								player.position.y = -height/5;
							}

							checkCollision(player);
						}

						

						render();		
				}


				var checkCollision = function(objectCollider) {

					if (objectCollider.position == orca.position) {
						console.log("foi");
					}
					// for (var vertexIndex = 0; vertexIndex < objectCollider.geometry.vertices.length; vertexIndex++)
					// {		
					// 	var localVertex = objectCollider.geometry.vertices[vertexIndex].clone();
					// 	var globalVertex = localVertex.applyMatrix4( objectCollider.matrix );
					// 	var directionVector = globalVertex.sub( objectCollider.position );
						
					// 	var ray = new THREE.Raycaster( originPoint, directionVector.clone().normalize() );
					// 	var collisionResults = ray.intersectObjects( collidableMeshList );
					// 	if ( collisionResults.length > 0 && collisionResults[0].distance < directionVector.length() ) 
					// 		console.log(" Hit ");
					// }	
				}

				window.addEventListener('load', function() {

					init();

				});
				
			</script>





			<script language="javascript" type="text/javascript">  
				$(document).ready(function(){
					//create a new WebSocket object.
					var wsUri = "ws://192.168.0.18:9000/demo/server.php"; 	
					websocket = new WebSocket(wsUri); 
					
					websocket.onopen = function(ev) { // connection is open 
						$('#message_box').append("<div class=\"system_msg\">Connected! Waiting for data.</div>");
					}

						
					//#### Message received from server?
					websocket.onmessage = function(ev) {

						msg = JSON.parse(ev.data);

						if ((clients.indexOf(msg.client) == -1) && (msg.client != undefined)) {
							clients.push(msg.client);
						} else {
							
						} 
						

						if (msg.data) {
							// $('#data_alpha').html(msg.data.alpha);
							// $('#data_beta').html(msg.data.beta);
							// $('#data_gamma').html(msg.data.gamma);

							// $('#data_acceleration_x').html(msg.data.acceleration_x);
							// $('#data_acceleration_y').html(msg.data.acceleration_y);
							// $('#data_acceleration_z').html(msg.data.acceleration_z);
							// $('#data_ralpha').html(msg.data.ralpha);
							// $('#data_rbeta').html(msg.data.rbeta);
							// $('#data_rgamma').html(msg.data.rgamma);

							if (firstLoad == true) {
								alphaIni = msg.data.alpha;
								betaIni = msg.data.beta;
								gammaIni = msg.data.gamma;
								xIni = msg.data.acceleration_x;
								yIni = msg.data.acceleration_y;
								xIni = msg.data.acceleration_z;
								firstLoad = false;
							}
							
						}
						
						var objDiv = document.getElementById("message_box");
						objDiv.scrollTop = objDiv.scrollHeight;
					};
					
					websocket.onerror	= function(ev){$('#message_box').append("<div class=\"system_error\">Error Occurred - "+ev.data+"</div>");}; 
					websocket.onclose 	= function(ev){$('#message_box').append("<div class=\"system_msg\">Connection Closed</div>");}; 
				});

				function degreeToRad(degree) {
					return degree * (Math.PI / 180);
				}



			</script>

			

			<div id="container">
			</div>

			<div class="chat_wrapper">
				<div class="message_box" id="message_box">
					Alpha = <span id="data_alpha"></span><br>
					Beta  = <span id="data_beta"></span><br>
					Gamma = <span id="data_gamma"></span><br><br>

					Acceleration X  = <span id="data_acceleration_x"></span><br>
					Acceleration Y  = <span id="data_acceleration_y"></span><br>
					Acceleration Z  = <span id="data_acceleration_z"></span><br>
					Rotate Alpha    = <span id="data_ralpha"></span><br>
					Rotate Beta     = <span id="data_rbeta"></span><br>
					Rotate Gamma    = <span id="data_rgamma"></span><br>
					Interval        = <span id="data_interval"></span><br><br>
				</div>
			</div>




	</body>
</html>